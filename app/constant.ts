export const OWNER = "Yidadaa";
export const REPO = "ChatGPT-Next-Web";
export const REPO_URL = `https://github.com/${OWNER}/${REPO}`;
export const ISSUE_URL = `https://github.com/${OWNER}/${REPO}/issues`;
export const UPDATE_URL = `${REPO_URL}#keep-updated`;
export const RELEASE_URL = `${REPO_URL}/releases`;
export const FETCH_COMMIT_URL = `https://api.github.com/repos/${OWNER}/${REPO}/commits?per_page=1`;
export const FETCH_TAG_URL = `https://api.github.com/repos/${OWNER}/${REPO}/tags?per_page=1`;
export const RUNTIME_CONFIG_DOM = "danger-runtime-config";
export const DEFAULT_API_HOST = "https://chatgpt1.nextweb.fun/api/proxy";

export enum Path {
  Home = "/",
  Chat = "/chat",
  Settings = "/settings",
  NewChat = "/new-chat",
  Masks = "/masks",
  Auth = "/auth",
}

export enum SlotID {
  AppBody = "app-body",
}

export enum FileName {
  Masks = "masks.json",
  Prompts = "prompts.json",
}

export enum StoreKey {
  Chat = "chat-next-web-store",
  Access = "access-control",
  Config = "app-config",
  Mask = "mask-store",
  Prompt = "prompt-store",
  Update = "chat-update",
  Sync = "sync",
}

export const MAX_SIDEBAR_WIDTH = 500;
export const MIN_SIDEBAR_WIDTH = 230;
export const NARROW_SIDEBAR_WIDTH = 100;

export const ACCESS_CODE_PREFIX = "ak-";

export const LAST_INPUT_KEY = "last-input";

export const REQUEST_TIMEOUT_MS = 60000;

export const EXPORT_MESSAGE_CLASS_NAME = "export-markdown";

export const OpenaiPath = {
  ChatPath: "v1/chat/completions",
  UsagePath: "dashboard/billing/usage",
  SubsPath: "dashboard/billing/subscription",
  ListModelPath: "v1/models",
};

export const DEFAULT_INPUT_TEMPLATE = `{{input}}`; // input / time / model / lang
export const DEFAULT_SYSTEM_TEMPLATE = `
You are ChatGPT, a large language model trained by OpenAI.
Knowledge cutoff: 2021-09
Current model: {{model}}
Current time: {{time}}`;

export const DEFAULT_MODELS = [
  {
    name: "gpt-4",
    available: true,
  },
  {
    name: "gpt-4-0613",
    available: true,
  },
  {
    name: "gpt-4-poe",
    available: true,
  },
  {
    name: "gpt-4-32k",
    available: true,
  },
  {
    name: "gpt-4-32k-poe",
    available: true,
  },
  {
    name: "gpt-3.5-turbo",
    available: true,
  },
  {
    name: "gpt-3.5-turbo-poe",
    available: true,
  },
  {
    name: "gpt-3.5-turbo-openai",
    available: true,
  },
  {
    name: "gpt-3.5-turbo-16k",
    available: true,
  },
  {
    name: "gpt-3.5-turbo-16k-poe",
    available: true,
  },
  {
    name: "gpt-3.5-turbo-16k-openai",
    available: true,
  },
  {
    name: "claude-instant-v1-100k",
    available: true,
  },
  {
    name: "claude-v1-100k",
    available: true,
  },
  {
    name: "claude-instant-v1",
    available: true,
  },
  {
    name: "claude-v1",
    available: true,
  },
    {
    name: "claude-instant-100k",
    available: true,
  },
  {
    name: "claude-instant",
    available: true,
  },
  {
    name: "claude-2",
    available: true,
  },
  {
    name: "claude-2-100k",
    available: true,
  },
  {
    name: "alpaca-7b",
    available: true,
  },
  {
    name: "stablelm-tuned-alpha-7b",
    available: true,
  },
  {
    name: "bloom",
    available: true,
  },
  {
    name: "bloomz",
    available: true,
  },
  {
    name: "flan-t5-xxl",
    available: true,
  },
  {
    name: "flan-ul2",
    available: true,
  },
  {
    name: "gpt-neox-20b",
    available: true,
  },
  {
    name: "oasst-sft-4-pythia-12b-epoch-3.5",
    available: true,
  },
  {
    name: "santacoder",
    available: true,
  },
  {
    name: "command-medium-nightly",
    available: true,
  },
  {
    name: "command-xlarge-nightly",
    available: true,
  },
  {
    name: "code-cushman-001",
    available: true,
  },
  {
    name: "code-davinci-002",
    available: true,
  },
  {
    name: "text-ada-001",
    available: true,
  },
  {
    name: "text-babbage-001",
    available: true,
  },
  {
    name: "text-curie-001",
    available: true,
  },
  {
    name: "text-davinci-002",
    available: true,
  },
  {
    name: "text-davinci-003",
    available: true,
  },
   {
    name: "palm2",
    available: true,
  },
  {
    name: "palm",
    available: true,
  },
  {
    name: "google",
    available: true,
  },
  {
    name: "google-bard",
    available: true,
  },
  {
    name: "google-palm",
    available: true,
  },
  {
    name: "bard",
    available: true,
  },
  {
    name: "falcon-40b",
    available: true,
  },
  {
    name: "falcon-7b",
    available: true,
  },
  {
    name: "llama-13b",
    available: true,
  },
  {
    name: "sage",
    available: true,
  }

] as const;
